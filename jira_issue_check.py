#!/opt/gitlab/embedded/bin/python3

import sys, os, http.client, json, re
from subprocess import run, PIPE
from urllib.parse import urlencode, quote_plus

g_jic_parms={}

def jira_email_from_jql(jql):
  conn = http.client.HTTPSConnection("edjira.ddns.net")
  
  payload = ""

  headers = {
      'cookie': "JSESSIONID=34A8034C261C0A9005451F6122A8AA14; atlassian.xsrf.token=BFGS-FJ78-9VJ1-XE5G%7C28da89293ec8c4b1d33b772413ed1c4d3a357fac%7Clin",
      'Content-Type': "application/json",
      'Authorization': "Basic R2l0TGFiLnRlc3RzOmR1bW15"
      }

  query_payload = {'maxResults':'1', 'jql':jql, 'fields':'assignee'}
  query_payload_encoded = urlencode(query_payload, quote_via=quote_plus)

  conn.request("GET", "/rest/api/2/search?"+query_payload_encoded, payload, headers)
  
  res = conn.getresponse()
  data = res.read()
  jira_response_object = json.loads(data)
  json_content = json.dumps(jira_response_object, indent=3, sort_keys=True)
  verbose_print(json_content)
  if 'issues' not in jira_response_object:
    verbose_print('jql failed')
    verbose_print('  '+jql)
    return 'jql-failed'
  if len(jira_response_object['issues']) != 1:
    verbose_print('jql failed to return one issue')
    verbose_print('  '+jql)
    return 'jql-failed-to-return-one-issue'
  if not jira_response_object['issues'][0]['fields']['assignee']:
    return 'issue-not-assigned'
  return jira_response_object['issues'][0]['fields']['assignee']['emailAddress']

def get_git_commit_refs(git_ref_from, git_ref_to):
  revlistprocess = run(['git','rev-list',git_ref_from+'..'+git_ref_to], stdout=PIPE, universal_newlines=True)
  commits_refs = revlistprocess.stdout.split()
  commits_refs.reverse()
  return commits_refs

def get_git_committer_email(commit_info):
  committer_line = commit_info[3]
  match = re.search('<([^>]+)>', committer_line)
  if not match:
    return 'no-email-found'
  return match.group(1)

def get_commit_info(commit_ref):
 # git cat-file commit 653690b2f847f387fc1a36c445be87c42401f616
 # 0 tree 813201bfecd55953d8b4bf87fec662a101bbba92
 # 1 parent 69a762c2998b97e868caab2bdc99e0030587ad15
 # 2 author Ed Slatt <eslatt@gitlab.com> 1631154727 -0400
 # 3 committer Ed Slatt <eslatt@gitlab.com> 1631154727 -0400
 # 4 
 # 5 test push

  catfileprocess = run(['git','cat-file','commit',commit_ref], stdout=PIPE, universal_newlines=True)
  return catfileprocess.stdout.split("\n")

def get_g_jic_parms(commit_ref):
  jic_parms = {"jira_issue_check_enabled":False}
  
  catfileprocess = run(['git','show',commit_ref+':.jira_issue_check.json'], stdout=PIPE, universal_newlines=True)
  if catfileprocess.returncode == 0:
    try:
      jic_parms = json.loads(catfileprocess.stdout)
    except ValueError:
      print("\n  Configuration file includes invalid json")
      exit(1)

  if "jira_issue_check_enabled" not in jic_parms:
    jic_parms["jira_issue_check_enabled"]=False

  if "author_validator_enabled" not in jic_parms:
    jic_parms["author_validator_enabled"]=False

  if "jira_jql" not in jic_parms:
    jic_parms["jira_jql"]=''

  if "verbose" not in jic_parms:
    if os.environ.get('jic_verbose_default')=='True':
      jic_parms["verbose"]=True
    else:
      jic_parms["verbose"]=False

  return jic_parms

def verbose_print(operand):
  global g_jic_parms
  if g_jic_parms['verbose']:
    print(operand)

def get_jira_issue_ids(commit_msg):
  pattern = re.compile(r'[A-Z]+-[0-9]+')
  return re.findall(pattern, commit_msg)

def main():
  global g_jic_parms
  print("Jira Issue Check")

  git_refs_strings = sys.stdin.readlines()
 # ignore lines 2-n of a multi-line push (TODO re-visit this)
  git_refs_string = git_refs_strings[0]
  (git_ref_from, git_ref_to, git_ref_branch) = git_refs_string.split(" ")

 # ignore special git cases (tags or new/deleted branches)
  if git_ref_from == "0000000000000000000000000000000000000000" or git_ref_to == "0000000000000000000000000000000000000000":
    # print("Excluded git push case (tag or new/deleted branch) ignored by design: " + git_refs_string)
    # above line can't use verbose_print since it requires not-yet-loaded g_jic_parms (which can't be loaded from tag or new/deleted branch pushes anyway)
    exit(0)
  commit_refs = get_git_commit_refs(git_ref_from, git_ref_to)

  g_jic_parms = get_g_jic_parms(commit_refs[-1])
  jic_parms_json = json.dumps(g_jic_parms, indent=3, sort_keys=True)
  verbose_print(commit_refs[-1])
  verbose_print(jic_parms_json)
  verbose_print("\n")

  on_fail_msg="\n  "+commit_refs[-1]+" configuration\n"
  on_fail_msg+=jic_parms_json+"\n"

  if g_jic_parms['jira_issue_check_enabled'] != True:
    exit(0)

  if len(commit_refs)>1:
    on_fail_msg+='  commits in push: '+' '.join(commit_refs)+"\n"

  verbose_print(commit_refs)
  for commit_ref in commit_refs:
    verbose_print(commit_ref)
    commit_info = get_commit_info(commit_ref)
    commit_msg = commit_info[5]
    verbose_print(commit_msg)
    jira_issue_ids = get_jira_issue_ids(commit_msg)
    verbose_print(jira_issue_ids)

    on_fail_msg+='  '+commit_ref+"\n"
    on_fail_msg+='    commit message: '+commit_msg+"\n"
    if len(jira_issue_ids)<1:
      on_fail_msg+="    no jira ids were found in the commit message\n"
      print(on_fail_msg)
      exit(1)

    at_least_one_jira_id_passes=False
    for jira_issue_id in jira_issue_ids:
      verbose_print(jira_issue_id)
      jql = "id="+jira_issue_id
      if len(g_jic_parms['jira_jql'])>0:
        jql += " and "+g_jic_parms['jira_jql']
      verbose_print(jql)
      jira_assignee_email = jira_email_from_jql(jql)
      verbose_print(jira_assignee_email)
      if (jira_assignee_email == 'jql-failed' or
          jira_assignee_email == 'jql-failed-to-return-one-issue'):
        on_fail_msg+='    following jql failed to return an issue: '+jql+"\n"
        continue # loops early leaving at_least_one_jira_id_passes False
      if g_jic_parms['author_validator_enabled'] != True:
        at_least_one_jira_id_passes=True
        break # we found a passing jira id so get out of the loop
      git_committer_email = get_git_committer_email(commit_info)
      verbose_print(git_committer_email)
      if git_committer_email == jira_assignee_email:
        at_least_one_jira_id_passes=True
        break # we found a passing jira id so get out of the loop

      on_fail_msg+='    git committer email <'+git_committer_email+'> != jira assignee email <'+jira_assignee_email+">\n"
      # jira_assignee_email == 'issue-not-assigned':

    if at_least_one_jira_id_passes != True:
      print(on_fail_msg)
      exit(1) # if any of the commits fail to have one valid jira id, decline the push

  exit(0) # all of the commits had at least one valid jira id so accept the push

main()